package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class ThirdActivity extends AppCompatActivity {
    private TextView username,address;
    private EditText amount,vat;
    private Button submit;
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;
    private String currentuserid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        mAuth= FirebaseAuth.getInstance();


        usersRef= FirebaseDatabase.getInstance().getReference().child("transaction");

        username=(TextView) findViewById(R.id.name);
        address=(TextView) findViewById(R.id.company);
        amount=(EditText) findViewById(R.id.amount);
        vat=(EditText) findViewById(R.id.vat);
        submit=(Button) findViewById(R.id.submit);
        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();
        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String user=dataSnapshot.child("username").getValue().toString();
                    String Time=dataSnapshot.child("time").getValue().toString();


                    username.setText("Name : " + user);




                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        usersRef= FirebaseDatabase.getInstance().getReference().child("company").child("Qvo5NQsir4OpaHRdzgaqcWLRfyC2");
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){


                    String Txn=dataSnapshot.child("txn").getValue().toString();

                    address.setText("Txn : " + Txn);



                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveRegisterInformation();
            }

            private void SaveRegisterInformation() {
                HashMap usermap= new HashMap();
                usermap.put("time", ServerValue.TIMESTAMP);
                usermap.put("balance","0");
                usermap.put("amount","200");
                usermap.put("vat","26");
                usermap.put("total","226");
                usersRef.push().setValue(usermap);
                usersRef.updateChildren(usermap).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                            //SendUserToMainActivity();
                            Toast.makeText(ThirdActivity.this,"Your transaction is saved successfully",Toast.LENGTH_SHORT).show();

                        }
                        else {
                            String message=task.getException().getMessage();
                            Toast.makeText(ThirdActivity.this,"Error :"+message,Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });

    }
}
