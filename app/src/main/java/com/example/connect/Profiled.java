package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Profiled extends AppCompatActivity {
    private DatabaseReference usersRef;
    private FirebaseAuth mAuth;
    private String currentuserid;
    private CardView cardview;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiled);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cardview=(CardView) findViewById(R.id.cardview);
        image=(ImageView) findViewById(R.id.image);
        final TextView a=(TextView) findViewById(R.id.username);
        final TextView b=(TextView) findViewById(R.id.balance);
        final TextView c=(TextView) findViewById(R.id.mobile);


        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();
        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String user=dataSnapshot.child("username").getValue().toString();
                    String balancee=dataSnapshot.child("Balance").getValue().toString();
                    String mobile=dataSnapshot.child("phone").getValue().toString();

                    a.setText("Username : " + user);
                    b.setText("Balance : " + balancee);
                    c.setText("Mobile : " + mobile);


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
