package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class FourthActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;
    private String currentuserid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(FourthActivity.this);
        alertDialog.setTitle("CONFIRM");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you sure you want to make Payment?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent d=new Intent(FourthActivity.this,MainActivity.class);
                startActivity(d);
                mAuth= FirebaseAuth.getInstance();
                currentuserid=mAuth.getCurrentUser().getUid();
                usersRef= FirebaseDatabase.getInstance().getReference("users").child(currentuserid);
                Query updateQuery=usersRef.child("users").orderByKey().equalTo("users");
                updateQuery.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().getParent().child("Balance").setValue("274");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent d=new Intent(FourthActivity.this,MainActivity.class);
                startActivity(d);

            }
        });
        alertDialog.show();

    }
}
